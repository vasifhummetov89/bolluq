<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\About\AboutResource;
use App\Http\Resources\Branch\BranchCollection;
use App\Http\Resources\Branch\BranchResource;
use App\Http\Resources\Brand\BrandCollection;
use App\Http\Resources\Certificate\CertificateCollection;
use App\Http\Resources\Director\DirectorResource;
use App\Http\Resources\Export\ExportResource;
use App\Http\Resources\History\HistoryCollection;
use App\Http\Resources\News\NewsCollection;
use App\Http\Resources\Partner\PartnerCollection;
use App\Http\Resources\Principle\PrincipleCollection;
use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Product\ProductResource;
use App\Http\Resources\Production\ProductionCollection;
use App\Http\Resources\Production\ProductionResource;
use App\Http\Resources\Setting\SettingResource;
use App\Http\Resources\SliderCollection;
use App\Http\Resources\StatisticCollection;
use App\Http\Resources\Structure\StructureCollection;
use App\Http\Resources\Structure\StructureResource;
use App\Http\Resources\Translation\TranslationCollection;
use App\Http\Resources\Type\TypeCollection;
use App\Mail\ContactMail;
use App\Mail\ExportMail;
use App\Models\About;
use App\Models\Branch;
use App\Models\Brand;
use App\Models\Certificate;
use App\Models\Chat;
use App\Models\Director;
use App\Models\Export;
use App\Models\History;
use App\Models\News;
use App\Models\Partner;
use App\Models\Principle;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Production;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\Statistic;
use App\Models\Structure;
use App\Models\Translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class HomeController extends Controller
{

    public function index()
    {
        return [
            'sliders'       => $this->sliders(),
            'partners'      => $this->partners(),
            'certificates'  => $this->certificates(),
            'news'          => $this->news(),
            'products'      => $this->products(),
            'statistics'    => $this->statistics(),
        ];
    }

    public function translation($locale){
        app()->setLocale($locale);

        $translations = Translation::all();
        $language     = [];

        foreach ($translations as $translation){
            $language[$translation->key] = $translation->value;
        }
        return $language;
    }

    public function chat(Request $request){
        $validator = Validator::make($request->all(),[
            'firstname' => 'required',
            'lastname'  => 'required',
            'email'     => 'required',
            'message'   => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => false,
                'errors'  => $validator->errors()
            ]);
        }


        $chat = Chat::create($request->all());

        return response()->json([
            'success' => true,
            'errors'  => []
        ]);
    }

    public function statistics(){
        $statistics = Statistic::with('translation')->get();
        return new StatisticCollection($statistics);
    }


    public function news(){
        $news = News::orderByDesc('id')->with('category')->limit(6)->get();
        return new NewsCollection($news);
    }

    public function sliders()
    {
        $sliders = Slider::orderByDesc('id')->get();
        return new SliderCollection($sliders);
    }

    public function partners()
    {
        $partners = Partner::orderBydesc('id')->get();
        return new PartnerCollection($partners);
    }

    public function certificates()
    {
        $certificates = Certificate::orderByDesc('id')->get();
        return new CertificateCollection($certificates);
    }

    public function about()
    {
        $about = About::orderByDesc('id')->first();
        return new AboutResource($about);
    }

    public function histories()
    {
        $histories = History::orderByDesc('id')->get();
        return new HistoryCollection($histories);
    }

    public function productions()
    {
        $productions = Production::orderByDesc('id')->get();
        return new ProductionCollection($productions);
    }

    public function production($slug)
    {
        $production = Production::whereRelation('translation', 'slug', $slug)->first();
        return new ProductionResource($production);
    }

    public function principles()
    {
        $principles = Principle::orderByDesc('id')->get();
        return new PrincipleCollection($principles);
    }

    public function structures()
    {
        $structures = Structure::limit(3)->orderByDesc('id')->whereNull('rewarding')->get();


        $monthly = Structure::where('rewarding', 1)->get();
        $yearly = Structure::where('rewarding', 2)->get();

        return  (new StructureCollection($structures))->additional([
            'monthly' => new StructureCollection($monthly),
            'yearly' => new StructureCollection($yearly),
        ]);

    }

    public function ourBrands()
    {

        $products = Product::orderBy('id', 'DESC')->whereNull('brand_id')->paginate(9);
        $dryFood = ProductCategory::where(['type' => 1, 'brand' => 1])->get();
        $confectionery = ProductCategory::where(['type' => 2, 'brand' => 1])->get();

        return (new ProductCollection($products))->additional([
            'dryFood' => new TypeCollection($dryFood),
            'confectionery' => new TypeCollection($confectionery),
        ]);

    }

    public function foreignBrands()
    {
        $brands = Brand::orderByDesc('id')->paginate(9);
        $dryFood = Brand::where(['type_id' => 1])->get();
        $confectionery = Brand::where(['type_id' => 2])->get();

        return (new BrandCollection($brands))->additional([
            'dryFood' => new TypeCollection($dryFood),
            'confectionery' => new TypeCollection($confectionery),
        ]);
    }

    public function foreignBrandProducts(Request $request, $slug)
    {
        $category = ProductCategory::whereRelation('translation', 'slug', $slug)->firstOrFail();

        $dryFood = ProductCategory::where(['type' => 1, 'brand' => 2])->get();
        $confectionery = ProductCategory::where(['type' => 2, 'brand' => 2])->get();

        return (new ProductCollection($category->products()->paginate(9)))->additional([
            'dryFood' => new TypeCollection($dryFood),
            'confectionery' => new TypeCollection($confectionery),
        ]);
    }

    public function foreign_brand_products(Request $request,$slug){
        $brand = Brand::whereRelation('translation', 'slug', $slug)->firstOrFail();
        $products = $brand->products()->paginate(9);

        $dryFood = Brand::where(['type_id' => 1])->get();
        $confectionery = Brand::where(['type_id' => 2])->get();

        return (new ProductCollection($products))->additional([
            'dryFood' => new TypeCollection($dryFood),
            'confectionery' => new TypeCollection($confectionery),
        ]);
    }

    public function foreignBrandProduct(Request $request, $slug, $product){
        $brand = Brand::whereRelation('translation', 'slug', $slug)->firstOrFail();
        $product = $brand->products()->whereRelation('translation','slug',$product)->firstOrFail();
        return new ProductResource($product);
    }

    public function foreignBrandProductDetail(Request $request, $product){
        $product = Product::whereRelation('translation','slug',$product)->firstOrFail();
        return new ProductResource($product);
    }

    public function categoryProducts($slug)
    {
        $category = ProductCategory::whereRelation('translation', 'slug', $slug)->firstOrFail();

        $dryFood = ProductCategory::where(['type' => 1, 'brand' => 1])->get();
        $confectionery = ProductCategory::where(['type' => 2, 'brand' => 1])->get();

        return (new ProductCollection($category->products()->paginate(9)))->additional([
            'dryFood' => new TypeCollection($dryFood),
            'confectionery' => new TypeCollection($confectionery),
        ]);
    }

    public function products()
    {
        $products = Product::orderByDesc('id')->limit(15)->get();
        return new ProductCollection($products);
    }

    public function sendMessage(Request $request){

        $validator = Validator::make($request->all(),[
            'firstname' => 'required',
            'lastname'  => 'required',
            'email'     => 'required|email',
            'phone'     => 'required',
            'message'   => 'required',
            'file'      => 'required|mimes:pdf,docx,doc',
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => false,
                'errors'  => $validator->errors()
            ]);
        }

        $send = "";
        $setting = Setting::latest()->first();
        $email = $setting ? $setting->email : null;

        if($email){

            if($request->hasFile('file')){
                $file = Storage::putFile('public/contact',$request->file('file'));
            }else{
                $file = null;
            }

            $data = array_merge($request->all(),['file' => $file]);
            $send = Mail::to($email)->send(new ContactMail($data));
        }

        return  ['success' => is_null($send) ? true : false,'errors' => []];

    }

    public function contact(){
        $setting = Setting::orderByDesc('id')->first();
        return $setting ? new SettingResource($setting) : ['data' => []];
    }

    public function branches(){
        $branches = Branch::all();
        $branches = buildTree($branches);
        return new BranchCollection($branches);
    }

    public function branch($slug){
        $branch = Branch::where('keyword',$slug)->with('parent')->firstOrFail();
        return new BranchResource($branch);
    }

    public function download($file = null){

        $extension = pathinfo($file,PATHINFO_EXTENSION);

        $headers = [
            'Content-Type' => config('mimes.'.$extension),
        ];

        $file = public_path('storage/'.$file);

        return response()->download($file, 'filename'.time().'.'.$extension, $headers);
    }

    public function export(){
        $export   = Export::latest()->first();
        $director = Director::latest()->first();
        return $export ? (new ExportResource($export))->additional([
            'director' => $director  ? new DirectorResource($director) :  []
        ]) : ['data' => [],'director' => $director  ? new DirectorResource($director) :  []];
    }

    public function sendExport(Request $request){

        $validator = Validator::make($request->all(),[
            'firstname' => 'required',
            'lastname'  => 'required',
            'email'     => 'required|email',
            'phone'     => 'required',
            'message'   => 'required',
            'file'      => 'required|mimes:pdf,docx,doc'
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors(),
                'success' => false
            ]);
        }
        $send = "";
        $setting = Setting::latest()->first();
        $email = $setting ? $setting->email : null;

        if($email){
            $file = Storage::putFile('public/export',$request->file('file'));
            $data = array_merge($request->all(),['file' => $file]);
            $send = Mail::to($email)->send(new ExportMail($data));
        }

        return  ['success' => is_null($send) ? true : false,'errors' => []];
    }

    public function search(Request $request){

        $validator = Validator::make($request->all(),[
            'query' => 'required'
        ]);

        if($validator->fails()){
            return [
                'success' => false,
                'message'  => $validator->errors()->first('query')
            ];
        }
        $query = $request->get('query');
        $querySlug = Str::slug($query);

        $product = Product::whereHas('translation',function($query) use ($querySlug){
           $query->where('slug','like','%'.$querySlug.'%');
        })->latest()->paginate(9)->withQueryString();

        return new ProductCollection($product);
    }

}
