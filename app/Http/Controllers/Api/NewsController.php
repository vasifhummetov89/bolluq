<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\News\NewsCollection;
use App\Http\Resources\News\NewsResource;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index(){
        $news = News::orderByDesc('id')->with('category')->paginate(9);
        return new NewsCollection($news);
    }

    public function news(Request $request,$slug){
        $news = News::whereRelation('translation','slug',$slug)->firstOrFail();
        return new NewsResource($news);
    }
}
