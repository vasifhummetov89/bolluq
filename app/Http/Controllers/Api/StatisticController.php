<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\StatisticCollection;
use App\Models\Statistic;
use Illuminate\Http\Request;

class StatisticController extends Controller
{
    public function index(){
        $statistics = Statistic::without('translation')->get();
        return new StatisticCollection($statistics);
    }
}
