<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Catalog\CatalogCollection;
use App\Http\Resources\Icon\IconCollection;
use App\Http\Resources\Occupation\OccupationCollection;
use App\Http\Resources\Position\PositionCollection;
use App\Http\Resources\Resource\ResourceCollection;
use App\Http\Resources\Resource\ResourceResource;
use App\Http\Resources\Vacancy\VacancyCollection;
use App\Http\Resources\Vacancy\VacancyResource;
use App\Models\Catalog;
use App\Models\Icon;
use App\Models\Occupation;
use App\Models\Position;
use App\Models\Resource;
use App\Models\Vacancy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VacancyController extends Controller
{
    public function index(Request $request){
        $vacancies = Vacancy::query();
        if($request->has('position')) $vacancies = $vacancies->whereIn('position_id',$request->position);
        if($request->has('occupation')) $vacancies = $vacancies->whereIn('occupation_id',$request->occupation);
        $vacancies = $vacancies->orderByDesc('id')->paginate(9);
        return new VacancyCollection($vacancies);

    }

    public function filter(){

        $positions = Position::orderByDesc('id')->get();
        $occupations = Occupation::orderByDesc('id')->get();

        return [
            'positions'     => new PositionCollection($positions),
            'occupations'   => new OccupationCollection($occupations)
        ];
    }

    public function vacancy($slug){
        $vacancy = Vacancy::whereRelation('translation','slug',$slug)->firstOrFail();
        return new VacancyResource($vacancy);
    }

    public function internship(){
        $resource = Resource::where('type',1)->orderByDesc('id')->first();
        $icons  = Icon::latest()->get();
        return $resource ? (new ResourceResource($resource))->additional([
            'icons' => new IconCollection($icons)
        ]) : ['data' => [],'icons' => new IconCollection($icons)];
    }

    public function rule(){
        $resource = Resource::where('type',2)->orderByDesc('id')->first();
        return $resource ? new ResourceResource($resource) : ['data' => []];
    }

    public function catalog(){
        $catalogs = Catalog::orderByDesc('id')->get();
        return new CatalogCollection($catalogs);
    }

    public function sendResume(Request $request, $vacancySlug){
        $vacancy = Vacancy::whereRelation('translation','slug',$vacancySlug)->firstOrFail();

        $validator = Validator::make($request->all(),[
            'firstname'             => 'required|max:255',
            'lastname'              => 'required|max:255',
            'father'                => 'required|max:255',
            'birthday'              => 'required|date',
            'birthplace'            => 'required',
            'citizenship'           => 'required',
            'image'                 => 'required|mimes:jpeg,jpg,png|max:3000',
            'address'               => 'required',
            'family'                => 'required',
            'military'              => 'required',
            'driving'               => 'required',
            'city_mobile'           => 'required|numeric',
            'email'                 => 'required|email',
            'city'                  => 'required|max:255',
            'secondary_education'   => 'required|max:255',
            'high_education'        => 'required',
            'faculty'               => 'required',
            'qualification'         => 'required',
            'year_of_admission'     => 'required',
            'expiration_year'       => 'required',
            'degree'                => 'required',
            'language'              => 'required',
            'computer_skill'        => 'required',


        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors(),
                'success' => false,
            ]);
        }







    }



}
