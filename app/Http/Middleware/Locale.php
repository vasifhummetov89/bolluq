<?php

namespace App\Http\Middleware;

use App\Models\Language;
use Closure;
use Illuminate\Http\Request;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $locale = apache_request_headers();

        if(isset($locale['locale'])){
            $chooseLanguage = Language::where(['locale' => $locale['locale'],'active' => 1])->first();
            if($chooseLanguage){
                app()->setLocale($chooseLanguage->locale);
            }else{
                $defaultLanguage = Language::where(['default' => 1,'active' => 1])->first();
                if($defaultLanguage){
                    app()->setLocale($defaultLanguage->locale);
                }else{
                    app()->setLocale(config('app.locale'));
                }
            }
        }else{
            $defaultLanguage = Language::where(['default' => 1,'active' => 1])->first();
            if($defaultLanguage){
                app()->setLocale($defaultLanguage->locale);
            }else{
                app()->setLocale(config('app.locale'));
            }
        }

        return $next($request);
    }
}
