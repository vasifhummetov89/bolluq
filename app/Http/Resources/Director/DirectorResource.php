<?php

namespace App\Http\Resources\Director;

use Illuminate\Http\Resources\Json\JsonResource;

class DirectorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'     => $this->id,
            'fax'    => $this->fax,
            'phone'   => $this->phone,
            'emails'   => explode(',',$this->email),
            'full_name' => $this->full_name,
            'address'   => $this->address,
            'position'   => $this->position,
            'mobile'     => $this->mobile
        ];
    }
}
