<?php

namespace App\Http\Resources\Principle;

use Illuminate\Http\Resources\Json\JsonResource;

class PrincipleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'title'         => $this->title,
            'description'   => $this->description,
            'image'         => asset('storage/'.$this->image)
        ];
    }
}
