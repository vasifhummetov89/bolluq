<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\Media\MediaCollection;
use App\Http\Resources\Media\MediaResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'image'     => asset('storage/'.$this->image),
            'galleryImage'=> asset('storage/'.$this->cover),
            'weight'    => $this->weight,
            'packaging' => $this->packaging,
            'code'      => $this->code,
            'title'     => $this->title,
            'slug'      => $this->slug,
            'images'    => new MediaCollection($this->images)
        ];
    }
}
