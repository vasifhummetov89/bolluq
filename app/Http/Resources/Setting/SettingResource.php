<?php

namespace App\Http\Resources\Setting;

use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'phone' => $this->phone,
            'email' => $this->email,
            'address' => [
                'name' => $this->address ,
                'link'    => $this->link
            ],
            'instagram'     => $this->instagram,
            'facebook'      => $this->facebook,
            'linkedin'      => $this->linkedin,
            'telegram'      => $this->telegram,
            'twitter'       => $this->twitter,
            'whatsapp'      => $this->whatsapp,

        ];
    }
}
