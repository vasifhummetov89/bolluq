<?php

namespace App\Http\Resources\Structure;

use App\Http\Resources\Position\PositionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class StructureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image' => asset('storage/'.$this->image),
            'fullName' => $this->full_name,
            'email'    => $this->email,
            'description' => $this->description,
            'position'    => new PositionResource($this->position)
        ];
    }
}
