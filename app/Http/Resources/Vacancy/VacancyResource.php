<?php

namespace App\Http\Resources\Vacancy;

use Illuminate\Http\Resources\Json\JsonResource;

class VacancyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'location'      => $this->location,
            'slug'          => $this->slug,
            'company'       => $this->company,
            'expired_at'    => $this->expired_at->format('d.m.Y'),
            'title'         => $this->title,
            'description'   => $this->when(request()->routeIs('vacancy'),$this->description),
            'requirement'   => $this->when(request()->routeIs('vacancy'),$this->requirement),
            'responsibility' => $this->when(request()->routeIs('vacancy'),$this->responsibility),
        ];
    }
}
