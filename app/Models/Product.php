<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Product extends Model implements TranslatableContract,HasMedia
{
    use HasFactory;
    use Translatable;
    use InteractsWithMedia;

    public $translatedAttributes = ['title','slug'];

    protected $fillable = ['brand_id','category_id','image','weight','packaging','code','cover'];

    public function brand(){
        return $this->belongsTo(Brand::class);
    }

    public function category(){
        return $this->belongsTo(ProductCategory::class);
    }

    public function images(){
        return $this->morphMany(Media::class,'imageable','model_type','model_id');
    }

}
