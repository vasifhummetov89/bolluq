<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Structure extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    public $translatedAttributes = ['description'];
    protected $fillable = ['image','email','position_id','full_name','rewarding','order'];

    public function position(){
        return $this->belongsTo(Position::class);
    }

    public function scopeEmployeeMonth($query){
        return $query->where('rewarding',2)->limit(1);
    }

    public function scopeEmployeeYear($query){
        return $query->where('rewarding',1)->limit(1);
    }
}
