<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DirectorTranslation extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = ['address'];

}
