<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IconTranslation extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['title'];
}
