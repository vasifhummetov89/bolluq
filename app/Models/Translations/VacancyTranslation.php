<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class VacancyTranslation extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::saving(fn($model) => $model->slug = Str::slug($model->title));
    }

}
