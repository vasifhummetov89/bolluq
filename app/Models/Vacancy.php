<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Vacancy extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    protected $casts = [
        'expired_at' => 'datetime'
    ];

    public $translatedAttributes = ['description','title','requirement','responsibility','slug'];
    protected $fillable = ['position_id','occupation_id','company','location','expired_at'];


    public function occupation(){
        return $this->belongsTo(Occupation::class);
    }

    public function position(){
        return $this->belongsTo(Position::class);
    }


}
