<?php

namespace App\Nova;

use Ek0519\Quilljs\Quilljs;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Vacancy extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Vacancy::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            Text::make('Company')->rules(['required']),
            Text::make('Location')->rules(['required']),
            BelongsTo::make('Position')->rules(['required']),
            BelongsTo::make('Occupation')->rules(['required']),
            Text::make('Title')->rules(['required','max:255'])->translatable(),
            Quilljs::make('Description')
                ->withFiles('public')
                ->rules(['required'])
                ->uploadUrlSplit('.')
                ->translatable(),
            Quilljs::make('Requirement')
                ->withFiles('public')
                ->rules(['required'])
                ->uploadUrlSplit('.')
                ->translatable(),
            Quilljs::make('Responsibility')
                ->withFiles('public')
                ->rules(['required'])
                ->uploadUrlSplit('.')
                ->translatable(),
            DateTime::make('Expired At')->rules(['required']),

        ];
    }
    public static function label() {
        return 'Vakansiyalar';
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
