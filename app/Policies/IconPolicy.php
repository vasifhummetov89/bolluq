<?php

namespace App\Policies;

use App\Models\Icon;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class IconPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return permission($user,'icon.view-any');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Icon  $icon
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Icon $icon)
    {
        return permission($user,'icon.view');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return permission($user,'icon.create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Icon  $icon
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Icon $icon)
    {
        return permission($user,'icon.update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Icon  $icon
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Icon $icon)
    {
        return permission($user,'icon.delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Icon  $icon
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Icon $icon)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Icon  $icon
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Icon $icon)
    {
        //
    }
}
