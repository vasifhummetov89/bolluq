<?php

namespace App\Policies;

use App\Models\Principle;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PrinciplePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return permission($user,'principe.view');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Principle  $principle
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Principle $principle)
    {
        return permission($user,'principe.view');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return permission($user,'principe.create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Principle  $principle
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Principle $principle)
    {
        return permission($user,'principe.update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Principle  $principle
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Principle $principle)
    {
        return permission($user,'principe.delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Principle  $principle
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Principle $principle)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Principle  $principle
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Principle $principle)
    {
        //
    }
}
