<?php

namespace App\Providers;

use App\Http\Resources\Translation\TranslationResource;
use App\Models\Language;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $data = [];
        if(Schema::hasTable('languages')){
            $languages = Language::all();

            foreach ($languages as $language){
                $data[$language->locale]  = $language->name;
            }

            if(!empty($data)){
                config(['nova-translatable.locales' => $data]);
                config(['translatable.locales' => array_keys($data)]);
            }
        }

//        TranslationResource::withoutWrapping();
    }
}
