<?php

use App\Models\User;

function buildTree($elements, $parentId = null) {
    $branch = [];

    foreach ($elements as $element) {
        if ($element->parent_id == $parentId) {
            $children = buildTree($elements, $element->id);
            if ($children) {
                $element->children = $children;
            }
            $branch[$element->id] = $element;
            unset($elements[$element->id]);
        }
    }
    return collect($branch);
}


function permission(User $user, $permission){

    if($user instanceof User){
        if($user->id == 1){
            return true;
        }else{
            return collect($user->role->permissions)->contains($permission);
        }
    }else{
        return 'Model not instance User';
    }
}
