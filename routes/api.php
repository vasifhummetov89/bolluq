<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('languages',[\App\Http\Controllers\Api\LanguageController::class,'index']);


Route::middleware('locale')->group(function (){

    Route::get('init',[\App\Http\Controllers\Api\HomeController::class,'index']);

    Route::get('statistics',[\App\Http\Controllers\Api\StatisticController::class,'index']);

    Route::get('sliders',[\App\Http\Controllers\Api\HomeController::class,'sliders']);

    Route::get('partners',[\App\Http\Controllers\Api\HomeController::class,'partners']);

    Route::get('certificates',[\App\Http\Controllers\Api\HomeController::class,'certificates']);

    Route::get('about',[\App\Http\Controllers\Api\HomeController::class,'about']);

    Route::get('histories',[\App\Http\Controllers\Api\HomeController::class,'histories']);

    Route::get('productions',[\App\Http\Controllers\Api\HomeController::class,'productions']);

    Route::get('production/{slug}',[\App\Http\Controllers\Api\HomeController::class,'production']);

    Route::get('principles',[\App\Http\Controllers\Api\HomeController::class,'principles']);

    Route::get('structures',[\App\Http\Controllers\Api\HomeController::class,'structures']);

    Route::get('products',[\App\Http\Controllers\Api\HomeController::class,'products']);

    Route::get('our-brands',[\App\Http\Controllers\Api\HomeController::class,'ourBrands']);

    Route::get('our-brands/{slug}',[\App\Http\Controllers\Api\HomeController::class,'categoryProducts']);

    Route::get('foreign-brands',[\App\Http\Controllers\Api\HomeController::class,'foreignBrands']);

    Route::get('foreign-brands/{slug}',[\App\Http\Controllers\Api\HomeController::class,'foreignBrandProducts']);

    Route::get('foreign-brand/{slug}',[\App\Http\Controllers\Api\HomeController::class,'foreign_brand_products']);

    Route::get('foreign-brands/{slug}/{product}',[\App\Http\Controllers\Api\HomeController::class,'foreignBrandProduct']);

    Route::get('product/{slug}',[\App\Http\Controllers\Api\HomeController::class,'foreignBrandProductDetail']);

    Route::get('news',[\App\Http\Controllers\Api\NewsController::class,'index']);

    Route::get('news/{slug}',[\App\Http\Controllers\Api\NewsController::class,'news']);

    Route::get('translation/{locale}',[\App\Http\Controllers\Api\HomeController::class,'translation']);

    Route::get('vacancies',[\App\Http\Controllers\Api\VacancyController::class,'index']);

    Route::post('vacancies',[\App\Http\Controllers\Api\VacancyController::class,'index']);

    Route::get('vacancies/{slug}',[\App\Http\Controllers\Api\VacancyController::class,'vacancy'])->name('vacancy');

    Route::get('internship',[\App\Http\Controllers\Api\VacancyController::class,'internship']);

    Route::get('rule',[\App\Http\Controllers\Api\VacancyController::class,'rule']);

    Route::get('vacancy-filter',[\App\Http\Controllers\Api\VacancyController::class,'filter']);

    Route::get('catalog',[\App\Http\Controllers\Api\VacancyController::class,'catalog']);

    Route::post('send-resume/{vacancy}',[\App\Http\Controllers\Api\VacancyController::class,'sendResume']);

    Route::get('contact',[\App\Http\Controllers\Api\HomeController::class,'contact']);

    Route::post('contact',[\App\Http\Controllers\Api\HomeController::class,'sendMessage']);

    Route::get('export',[\App\Http\Controllers\Api\HomeController::class,'export']);

    Route::post('export',[\App\Http\Controllers\Api\HomeController::class,'sendExport']);


    Route::get('branches',[\App\Http\Controllers\Api\HomeController::class,'branches']);

    Route::get('branches/{slug}',[\App\Http\Controllers\Api\HomeController::class,'branch']);

    Route::get('download/{file?}',[\App\Http\Controllers\Api\HomeController::class,'download']);

    Route::get('setting',[\App\Http\Controllers\Api\SettingController::class,'index']);

    Route::get('search',[\App\Http\Controllers\Api\HomeController::class,'search']);

    Route::post('chat',[\App\Http\Controllers\Api\HomeController::class,'chat']);

});


